<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Customer;
use Illuminate\Http\Request;
use App\Check_email;
use App\BusinessSetting;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\WelcomeEmail;
use App\Mail\Verfication;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
     public function send_email_v(Request $request){
         $check=User::where("email","=",$request->email)->first();
         if(empty($check)){
             $data=Check_email::where("email","=",strtolower($request->email))->first();
             $random_str=Str::random(10);
             if($data){
                 $insert=["email_token"=>$random_str];
                 $done=Check_email::where("email","=",strtolower($request->email))->update($insert);
             }
             else{
                 $insert=["email"=>strtolower($request->email),"email_token"=>$random_str];
                 $done=Check_email::insert($insert);
                }
                 if($done){
                     Mail::to($data['email'])->send(new Verfication($random_str));
                     return response()->json(["status"=>1,"email"=>$request->email]);
                 }
                 else{
                     return response()->json(["status"=>0,"error"=>"try again"]);
                 }
             
         }
         else{
             return response()->json(["status"=>2,"error"=>"Email already taken"]);
         }
     }
      public function verify_code(Request $request){
            $data=Check_email::where("email","=",strtolower($request->email))->where("email_token","=",$request->code)->first();
            //dd($data);
             if($data){
            return response()->json(["status"=>1,"email"=>$request->email]);
            }
            else{
            return response()->json(["status"=>0,"error"=>"UnAuthorization"]);
                 }
            }
       protected function validator(array $data)
       {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
       }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            "phone"=>$data["phone"]
        ]);
        Mail::to($data['email'])->send(new WelcomeEmail($user));
        if(BusinessSetting::where('type', 'email_verification')->first()->value != 1){
            $user->email_verified_at = date('Y-m-d H:m:s');
            $user->save();
            flash(__('Registration successfull.'))->success();
        }
        else {
            flash(__('Registration successfull. Please verify your email.'))->success();
        }

        $customer = new Customer;
        $customer->user_id = $user->id;
        $customer->save();
        \Session::flash('new_user', true);
        return $user;
    }
}
