@extends('frontend.layouts.app')

@section('content')
    <section class="gry-bg py-4">
        <div class="profile">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 offset-xl-1" id="email_form" style="display:none;">
                        <div class="card">
                            <div class="text-center px-35 pt-5">
                                <h3 class="heading heading-4 strong-500">
                                    Verify Your Email
                                </h3>
                            </div>
                          
                            <div class="px-5 py-3 py-lg-5">
                                <span class="error" style="color: red;position: relative:left: 214px; top: -12px;"></span>
                                <div class="row align-items-center">
                                    
                                    <div class="col-6 col-lg">
                                        <form class="form-default" id="check_e" action="{{ route('verify-email-t') }}" method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="col-6 m-auto">
                                                    <div class="form-group">
                                                        <div class="input-group input-group--style-1">
                                                            <input type="email" name="email" placeholder="Enter your email" style="width: 88%;" required >
                                                            <span class="input-group-addon">
                                                                <i class="text-md la la-user"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="row align-items-center">
                                               <div class="col-8"style="position: relative;left: 214px;top: -12px;">
                                                    <a class="toggle_f" style="color: #F79F1F; cursor:pointer"  data-hide="#email_form" data-show="#step-1">Verfication with Mobile?</a>
                                                </div>
                                                <div class="col-6 text-right  mt-3 m-auto">
                                                    <button type="submit" id="e" value="submit" name="submit" class="btn btn-styled btn-base-1 w-100 btn-md">Send Code</button>
                                                </div>
                                            </div>
                                        </form>
                                      
                            </div>
                           
                             </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-xl-10 offset-xl-1" id="verify-email-f" style="display:none">
                        <div class="card">
                            <div class="text-center px-35 pt-5">
                                <h3 class="heading heading-4 strong-500">
                                    Verify Your Email Code
                                </h3>
                            </div>
                          
                            <div class="px-5 py-3 py-lg-5">
                                <span class="error" style="color: red;position: relative:left: 214px; top: -12px;"></span>
                                <div class="row align-items-center">
                                    <div class="col-6 col-lg">
                                        <form class="form-default" id="verify_e" action="{{ route('verify_code') }}" method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="col-6 m-auto">
                                                    <div class="form-group">
                                                        <div class="input-group input-group--style-1">
                                                            <input type="text"  name="code" placeholder="Enter your code" style="width: 88%;" required >
                                                            <input type="hidden" name="email" value="email" id="c_email">
                                                            <span class="input-group-addon">
                                                                <i class="text-md la la-user"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="row align-items-center">
                                               <!--<div class="col-8" style="position: relative;left: 214px;top: -12px;">-->
                                               <!--     <a class="toggle_f" style="color: #F79F1F; cursor:pointer" data-show="#email_form" data-hide="#step-1">Verfication with Email?</a>-->
                                               <!-- </div>-->
                                                <div class="col-6 text-right  mt-3 m-auto">
                                                    <button type="submit" id="e-c"  value="submit" name="submit" class="btn btn-styled btn-base-1 w-100 btn-md">Send Code</button>
                                                </div>
                                            </div>
                                        </form>
                                      
                            </div>
                           
                             </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-xl-10 offset-xl-1" id="step-1">
                        <div class="card">
                            <div class="text-center px-35 pt-5">
                                <h3 class="heading heading-4 strong-500">
                                    Verify Your Mobile Number
                                </h3>
                            </div>
                          
                            <div class="px-5 py-3 py-lg-5">
                                <div class="row align-items-center">
                                    <div class="col-6 col-lg">
                                        <form class="form-default" id="check_p" action="{{ route('check_phone') }}" method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="col-6 m-auto">
                                                    <div class="form-group">
                                                        <div class="input-group input-group--style-1">
                                                            <input type="tel" id="phone" name="phone" placeholder="+92333xxxxxxx" value="+92"  maxlength="13" maxlength="13" style="width: 88%;" required >
                                                            <span class="input-group-addon">
                                                                <i class="text-md la la-user"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="row align-items-center">
                                               <div class="col-8" style="position: relative;left: 214px;top: -12px;">
                                                    <a class="toggle_f" style="color: #F79F1F; cursor:pointer" data-show="#email_form" data-hide="#step-1">Verfication with Email?</a>
                                                </div>
                                                <div class="col-6 text-right  mt-3 m-auto">
                                                    <button type="submit" id="c" value="submit" name="submit" class="btn btn-styled btn-base-1 w-100 btn-md">Send Code</button>
                                                </div>
                                            </div>
                                        </form>
                                      
                            </div>
                           
                             </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-xl-10 offset-xl-1" id="step-2" style="display:none">
                        <div class="card">
                            <div class="text-center px-35 pt-5">
                                <h3 class="heading heading-4 strong-500">
                                    Code Verfication
                                </h3>
                            </div>
                          
                            <div class="px-5 py-3 py-lg-5">
                                <div class="row align-items-center">
                                    <div class="col-12 col-lg">
                                        
                                            
                                            <div class="row">
                                                <div class="col-6 m-auto">
                                                    <div class="form-group">
                                                        <!-- <label>{{ __('name') }}</label> -->
                                                        <div class="input-group input-group--style-1">
                                                            <input type="text" class="form-control"  placeholder="Enter Code" id="code" style="width: 88%;" required>
                                                            <span class="input-group-addon">
                                                                <i class="text-md la la-user"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="row align-items-center">
                                                <div class="col-6 text-right  mt-3 m-auto">
                                                    <button type="submit" id="proceed" class="btn btn-styled btn-base-1 w-100 btn-md">Verify</button>
                                                </div>
                                            </div>
                                        
                                      
                            </div>
                             </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-xl-10 offset-xl-1" id="step-3" style="display:none">
                        <div class="card">
                            <div class="text-center px-35 pt-5">
                                <h3 class="heading heading-4 strong-500">
                                    {{__('Create an account.')}}
                                </h3>
                            </div>
                            @if ($errors->any())
                        <div class="alert alert-danger mx-2 mt-2">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                            <div class="px-5 py-3 py-lg-5" style="width: 70%;align-self: center;">
                                <div class="row align-items-center">
                                    <div class="col-12 col-lg">
                                        <form class="form-default" id="r-f" role="form" action="{{ route('register') }}" method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <!-- <label>{{ __('name') }}</label> -->
                                                        <div class="input-group input-group--style-1">
                                                            <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" placeholder="{{ __('Name') }}" name="name" required>
                                                            <span class="input-group-addon">
                                                                <i class="text-md la la-user"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <!-- <label>{{ __('email') }}</label> -->
                                                        <div class="input-group input-group--style-1">
                                                            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{ __('Email') }}" name="email" id="email" required>
                                                            <span class="input-group-addon">
                                                                <i class="text-md la la-envelope"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
<input type="hidden" value="" name="phone" id="phone_nu">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <!-- <label>{{ __('password') }}</label> -->
                                                        <div class="input-group input-group--style-1">
                                                            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Password') }}" name="password" required>
                                                            <span class="input-group-addon">
                                                                <i class="text-md la la-lock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <!-- <label>{{ __('confirm_password') }}</label> -->
                                                        <div class="input-group input-group--style-1">
                                                            <input type="password" class="form-control" placeholder="{{ __('Confirm Password') }}" name="password_confirmation" required>
                                                            <span class="input-group-addon">
                                                                <i class="text-md la la-lock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_KEY') }}">
                                                            @if ($errors->has('g-recaptcha-response'))
                                                                <span class="invalid-feedback" style="display:block">
                                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="checkbox pad-btm text-left">
                                                        <input class="magic-checkbox" type="checkbox" name="checkbox_example_1" id="checkboxExample_1a" required>
                                                        <label for="checkboxExample_1a" class="text-sm">{{__('By signing up you agree to our terms and conditions.')}}</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row align-items-center">
                                                <div class="col-12 text-right  mt-3">
                                                    <button type="submit" class="btn btn-styled btn-base-1 w-100 btn-md">Create Account</button>
                                                </div>
                                            </div>
                                        </form>
                                      
                                     
                                        
                                    </div>
                                    
                                   
                                    <!--<div class="col-12 col-lg">-->
                                    <!--    @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1)-->
                                    <!--        <a href="{{ route('social.login', ['provider' => 'google']) }}" class="btn btn-styled btn-block btn-google btn-icon--2 btn-icon-left px-4 my-4">-->
                                    <!--            <i class="icon fa fa-google"></i> {{__('Login with Google')}}-->
                                    <!--        </a>-->
                                    <!--    @endif-->
                                    <!--    @if (\App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1)-->
                                    <!--        <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="btn btn-styled btn-block btn-facebook btn-icon--2 btn-icon-left px-4 my-4">-->
                                    <!--            <i class="icon fa fa-facebook"></i> {{__('Login with Facebook')}}-->
                                    <!--        </a>-->
                                    <!--    @endif-->
                                    <!--    @if (\App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)-->
                                    <!--    <a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="btn btn-styled btn-block btn-twitter btn-icon--2 btn-icon-left px-4 my-4">-->
                                    <!--        <i class="icon fa fa-twitter"></i> {{__('Login with Twitter')}}-->
                                    <!--    </a>-->
                                    <!--    @endif-->
                                    <!--</div>-->
                                </div>
                            </div>
                            <div class="text-center px-35 pb-3">
                                <p class="text-md">
                                    {{__('Already have an account?')}}<a href="{{ route('user.login') }}" class="strong-600">{{__('Log In')}}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                     

                </div>
            </div>
        </div>
    </section>
    
@endsection

@section('script')
    <script type="text/javascript">
        function autoFillSeller(){
            $('#email').val('seller@example.com');
            $('#password').val('123456');
        }
        function autoFillCustomer(){
            $('#email').val('customer@example.com');
            $('#password').val('123456');
        }
        @if($errors->any())
        $('#step-1').hide();
        $('#step-3').show();
        @endif
       
    </script>
<script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.19.1/firebase-auth.js"> </script>
<script>
 
  var firebaseConfig = {
    apiKey: "AIzaSyA_ZexHljTTP-Kmny2QtimUQHdAFov2nTc",
    authDomain: "trigonsoft-project.firebaseapp.com",
    databaseURL: "https://trigonsoft-project.firebaseio.com",
    projectId: "trigonsoft-project",
    storageBucket: "trigonsoft-project.appspot.com",
    messagingSenderId: "720006640137",
    appId: "1:720006640137:web:f16a96459c6647b1be3ed8",
    measurementId: "G-T5FPQM2EH2"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
</script>
<script>
    function rec() {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('c', {
            'size': 'invisible',
            'callback': function(response) {
                onSignInSubmit();
            }
        });
    }
    
    var coderesult = "";
    var phone_num="";
    var email="";
    $(document).on("click",".toggle_f",function(e){
       var hide=$(this).attr("data-hide");
       var show=$(this).attr("data-show");
       $(hide).hide();
       $(show).show();
    });
    $(document).on("submit","#check_p",function(e) {
        e.preventDefault();
        phone_num=$("#phone").val();
        var url = $(this).attr("action");
            $("#c").prop("disabled", true);
            $("#c").html("sending...");
            var form = new FormData(this);

            $.ajax({
                method: "post",
                url: url,
                DataType: "json",
                data: form,
                contentType: false,
                processData: false,
                success: function(data) {

                    if (data.status == 1) {
                        rec();
                        phone_num=data.num;
                        firebase.auth().signInWithPhoneNumber(phone_num, window.recaptchaVerifier).then(function(confirmationResult) {
                            window.confirmationResult = confirmationResult;
                            coderesult = confirmationResult;
                            $('#step-1').hide();
                            $('#step-2').show();
                        }).catch(function(error) {
                            alert(error.message);
                        });

                    } else {

                        alert("this phone number is already exist!Please try another number");
                        $("#c").prop("disabled", false);
                        $("#c").html("Verify");
                    }
                }
            });

        });
        
        $(document).on("submit","#check_e",function(e) {
        e.preventDefault();
        var url = $(this).attr("action");
        var form = new FormData(this);
        var cur=$(this);
         $("#e").prop("disabled", true);
         $("#e").html("sending...");
            $.ajax({
                method: "post",
                url: url,
                DataType: "json",
                data: form,
                contentType: false,
                processData: false,
                success: function(data) {
                if (data.status == 1) {
                    $("#c_email").val(data.email);
                    $("#verify-email-f").show();
                    $("#email_form").hide();
                    }
                    else if(data.status==2) {
                      $("#e").prop("disabled", false);
                      $("#e").html("send");
                      cur.closest(".col-xl-10").find(".error").html(data.error);
                    }
                    
                }
            });

        });
        var email;
        $(document).on("submit","#verify_e",function(e) {
        e.preventDefault();
        var url = $(this).attr("action");
        var form = new FormData(this);
        var cur=$(this);
         $("#e-c").prop("disabled", true);
         $("#e-c").html("verfiying...");
            $.ajax({
                method: "post",
                url: url,
                DataType: "json",
                data: form,
                contentType: false,
                processData: false,
                success: function(data) {
                if (data.status == 1) {
                    $("#verify-email-f").hide();
                    $("#step-3").show();
                    $("#r-f").append("<input type='hidden' name='email' value='"+data.email+"'>");
                    $("#email").val(data.email);
                    $("#email").prop("disabled",true);
                    }else {
                     $("#e-c").prop("disabled", true);
                     $("#e-c").html("verfiying...");
                     cur.closest(".col-xl-10").find(".error").html(data.error);
                       
                    }
                }
            });

        });
        $("#proceed").click(function(e) {
            var num = $("#code").val();
            $(this).text("verifying....");
            coderesult.confirm(num).then(function(result) {
                console.log(result);
                $('#step-1').hide();
                $('#step-2').hide();
                $('#step-3').show();
                $("#phone_nu").val(phone_num)
            }).catch(function(error) {
                alert(error.message);
            });
        });
</script>
@endsection
