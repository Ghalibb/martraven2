@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <div class="row contact-seller-page" >

        <div class="col-12 pt-2 d-flex seller-img">
            <i class="fa fa-user"></i>
            <h5>Anas mughal</h5>
        </div>
        <div class="col-12">
            <div class="form-group">
                <input class="form-control mb-3" placeholder="Enter your subject" />
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" placeholder="Enter your product detail like color,size etc."></textarea>
            </div>
            <form class="mb-2 mobil-attachmntbtn">
                <strong class="form-group" for="myfile">Add Attachment:</strong>
                <input type="file" id="myfile" name="myfile"><br>

            </form>
            <button type="button" class="btn btn-block btn-base-1 btn-circle btn-icon-left mt-2 mobil-sendquery-btn" style="border-radius: 0!important; max-width: 180px;margin-left: auto">
              Send inquiry now
            </button>
            <hr/>
        </div>
        <div class="col-12">
            <div class="form-check mb-2">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <strong class="form-check-label" for="defaultCheck1">
                  <strong> Agree to Share Business card with Seller</strong>
                </strong>
            </div>
            <label class="form-group">
                Please make sure your contact information is correct (View and Edit). Your message will be sent directly to the recipient(s) and will not be publicly displayed. Note that if the recipient is a Gold Supplier, they can view your contact information, including your registered email address. Alibaba will never distribute or sell your personal information to third parties without your express permission.
            </label>
        </div>


    </div>
</div>


@endsection
