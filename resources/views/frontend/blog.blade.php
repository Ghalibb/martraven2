@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8 blogsection">
            <div class="row m-0">
                <div class="col-12 blogimage p-0">
                    <img src="{{ asset('resources/views/images/Image.jpg') }}" class="img-fluid">
                </div>
                <div class="col-8 blogtitle ">
                    Cartridge Is Better Than Ever
                    A Discount Toner

                </div>
                <div class="col-12 blogwriter p-0">
                    <div class="row m-0">
                        <div class="col-11 writerNAme p-0">
                            <h6 class="m-0">Zoe Charlton</h6>
                            <p class="m-0">12 Dec, 2020  |  11:21 am</p>
                        </div>
                        <div class="col-1 picdiv p-0">
                            <div></div>

                        </div>

                    </div>

                </div>
                <div class="col-12 blogdetail">
                    <p > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum enim facilisis gravida neque convallis a cras semper auctor. Gravida dictum fusce ut placerat.</p>

                    <p> Habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim. Etiam erat velit scelerisque in. Nibh sed pulvinar proin gravida. Ipsum dolor sit amet consectetur adipiscing. Habitant morbi tristique senectus et netus et malesuada fames. Viverra justo nec ultrices dui sapien eget mi proin sed. commodo. Id semper risus in hendrerit dolor magna.</p>

                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum enim facilisis gravida neque convallis a cras semper auctor. Gravida dictum fusce ut placerat. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim. Etiam erat velit scelerisque in. Nibh sed pulvinar proin gravida. Ipsum dolor sit amet consectetur adipiscing. Habitant morbi tristique senectus et netus et malesuada fames. Viverra justo nec ultrices dui sapien eget mi proin sed. Et malesuada fames ac turpis egestas maecenas. Sit amet facilisis magna etiam tempor orci. Lorem sed risus ultricies tristique nulla aliquet enim tortor at. Sit amet dictum sit amet justo donec enim diam. Risus ultricies tristique nulla aliquet enim. Eget dolor morbi non arcu risus quis varius quam. Amet aliquam id diam maecenas ultricies mi.</p>

                    <p>Risus feugiat in ante metus dictum at. Dui nunc mattis enim ut. Consequat interdum varius sit amet mattis vulputate. Faucibus turpis in eu mi. At risus viverra adipiscing at in. Eu feugiat pretium nibh ipsum consequat nisl vel. Arcu dui vivamus arcu felis. Massa enim nec dui nunc mattis. In iaculis nunc sed augue lacus viverra. Consequat interdum varius sit amet mattis. Interdum posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Leo in vitae turpis massa.</p>

                    <p>Quam nulla porttitor massa id neque aliquam vestibulum morbi blandit. Quis viverra nibh cras pulvinar mattis nunc sed. Commodo odio aenean sed adipiscing diam donec adipiscing tristique. Donec pretium vulputate sapien nec sagittis aliquam malesuada bibendum. Mattis ullamcorper velit sed ullamcorper. Vel fringilla est ullamcorper eget nulla facilisi etiam dignissim. Netus et malesuada fames ac turpis. Laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Pellentesque habitant morbi tristique senectus et netus. Nullam vehicula ipsum a arcu. Ornare aenean euismod elementum nisi quis eleifend. Velit sed ullamcorper morbi tincidunt ornare. Luctus venenatis lectus magna fringilla. Morbi quis commodo odio aenean.</p>

                    <p>A lacus vestibulum sed arcu non odio euismod lacinia. Dictum non consectetur a erat nam at lectus urna. Molestie nunc non blandit massa enim nec dui nunc mattis. Mi tempus imperdiet nulla.</p>
                </div>
                <div class="col-12 p-0"><hr></div>
                <div class="col-6 commentsection p-0">
                    <img src="{{ asset('resources/views/images/25663.png') }}" class="img-fluid">

                    06 Comments
                </div>
                <div class="col-6 likesection p-0">
                    <img src="{{ asset('resources/views/images/Heart.png') }}" class="img-fluid">

                    Lily and 4 people like this
                </div>
                <div class="col-12 comment1section" style="padding-top: 60px;">
                    <p>Amazing piece. Super comfortable to kick back on or for propping up my feet. Color is true to image. Love it!</p>

                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-1 clientimages">
                            <img src="{{ asset('resources/views/images/1.png') }}" class="img-fluid">

                        </div>
                        <div class="col-6 clientName p-0">
                            <h6>John Smith, Architector</h6>
                            <p>Los Angeles, CA</p>
                        </div>
                    </div>
                </div>

                <div class="col-12"><hr></div>
                <div class="col-12 comment1section" >
                    <p>Amazing piece. Super comfortable to kick back on or for propping up my feet. Color is true to image. Love it!</p>

                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-1 clientimages">
                            <img src="{{ asset('resources/views/images/2.png') }}" class="img-fluid">

                        </div>
                        <div class="col-6 clientName p-0">
                            <h6>John Smith, Architector</h6>
                            <p>Los Angeles, CA</p>
                        </div>
                    </div>
                </div>
                <div class="col-12"><hr></div>
                <div class="col-12 comment1section">
                    <p>Amazing piece. Super comfortable to kick back on or for propping up my feet. Color is true to image. Love it!</p>

                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-1 clientimages">
                            <img src="{{ asset('resources/views/images/1.png') }}" class="img-fluid">
                        </div>
                        <div class="col-6 clientName p-0">
                            <h6>John Smith, Architector</h6>
                            <p>San Francisco, CA</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 leavcomment">
                    Leave a Comment
                </div>
                <div class="col-12 commentform">
                    <form>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" placeholder="First name">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" placeholder="Last name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" placeholder="Subject">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Message" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 postcomment">
                                <button class="btn">Post Comment</button>
                            </div>
                        </div>
                    </form>
                </div>



            </div>
        </div>
        <div class="col-4 blogsection">
            <div class="row m-0">
                <div class="col-12 searchitem">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Search" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-0 popularblog">
                <div class="col-12">
                    <div class="row headingmargin">
                        <div class="col-6 blogheading">Popular Blogs</div>
                        <div class="col-4"><hr></div>
                    </div>
                </div>
                <div class="col-12 blogcard">
                    <div class="card" >
                        <img src="{{ asset('resources/views/images/1.jpg') }}" class="img-fluid">

                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 blogcardheading">
                                    Cartridge Is Better Than Ever
                                    A Discount Toner
                                </div>
                                <div class="col-12 blogwritername">Mate Winston  |  Dec 2020</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 blogcard">
                    <div class="card" >
                        <img src="{{ asset('resources/views/images/2.jpg') }}" class="img-fluid">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 blogcardheading">
                                    Cartridge Is Better Than Ever
                                    A Discount Toner
                                </div>
                                <div class="col-12 blogwritername">Mate Winston  |  Dec 2020</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 blogcard">
                    <div class="card" >
                        <img src="{{ asset('resources/views/images/3.jpg') }}" class="img-fluid">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 blogcardheading">
                                    Cartridge Is Better Than Ever
                                    A Discount Toner
                                </div>
                                <div class="col-12 blogwritername">Mate Winston  |  Dec 2020</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 blogcard">
                    <div class="card" >
                        <img src="{{ asset('resources/views/images/4.jpg') }}" class="img-fluid">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 blogcardheading">
                                    Cartridge Is Better Than Ever
                                    A Discount Toner
                                </div>
                                <div class="col-12 blogwritername">Mate Winston  |  Dec 2020</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


@endsection
